package com.rakbanktask;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;

import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;

public class DeviceManagerModule extends ReactContextBaseJavaModule {
    @NonNull
    @Override
    public String getName() {
        return "DeviceManager";
    }

    private static final String PREVENT_SCREENSHOT_ERROR_CODE = "PREVENT_SCREENSHOT_ERROR_CODE";
    private final ReactApplicationContext reactContext;

    DeviceManagerModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }
    @ReactMethod
    public void getDeviceName(Promise promise) {
        try {
            String bluetoothName = Settings.Secure.getString(getReactApplicationContext().getContentResolver(), "bluetooth_name");
            if (bluetoothName != null) {
                promise.resolve(bluetoothName);
            }

            if (Build.VERSION.SDK_INT >= 25) {
                String deviceName = Settings.Global.getString(getReactApplicationContext().getContentResolver(), Settings.Global.DEVICE_NAME);
                if (deviceName != null) {
                    promise.resolve(deviceName);
                }
            }
        } catch (Exception e) {
            // same as default unknown return
        }
        promise.resolve("unknown");
    }

    @ReactMethod
    public void getIpAddress(Promise promise) {
        try {
           promise.resolve(InetAddress.getByAddress(
                   ByteBuffer
                           .allocate(4)
                           .order(ByteOrder.LITTLE_ENDIAN)
                           .putInt(Objects.requireNonNull(getWifiInfo()).getIpAddress())
                           .array())
                   .getHostAddress());

        } catch (Exception e) {
            promise.resolve("unknown");
        }
    }

    private WifiInfo getWifiInfo() {
        WifiManager manager = (WifiManager) getReactApplicationContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (manager != null) {
            return manager.getConnectionInfo();
        }
        return null;
    }

    @ReactMethod
    public void forbid(Promise promise) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Objects.requireNonNull(getCurrentActivity()).getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
                    promise.resolve("Done. Screenshot taking locked.");
                } catch(Exception e) {
                    promise.reject(PREVENT_SCREENSHOT_ERROR_CODE, "Forbid screenshot taking failure.");
                }
            }
        });
    }

    @ReactMethod
    public void allow(Promise promise) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Objects.requireNonNull(getCurrentActivity()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
                    promise.resolve("Done. Screenshot taking unlocked.");
                } catch (Exception e) {
                    promise.reject(PREVENT_SCREENSHOT_ERROR_CODE, "Allow screenshot taking failure.");
                }
            }
        });
    }
}

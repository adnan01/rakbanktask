import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Image,
  StatusBar,
  NativeModules,
  Platform
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import CustomButton, { ButtonType } from './src/components/customButton';

const DeviceManager = NativeModules.DeviceManager;

const App: React.FC = () => {
  const [buttonState, updateButtonState] = useState<ButtonType>("activate");
  const [isScreenShotEnable, updateScreenShotState] = useState<boolean>(true);

  useEffect(() => {
    Platform.OS === 'android' && toggleScreenShot();
  }, [isScreenShotEnable]);

  const toggleScreenShot = async () => {
    if (isScreenShotEnable) {
      await DeviceManager.forbid();
      updateButtonState("activated");
    } else {
      await DeviceManager.allow();
      updateButtonState("activate");
    }
  }

  const onPress = async () => {
    updateButtonState("waiting");
    const deviceName =  await DeviceManager.getDeviceName();

    if (Platform.OS === 'android') {
      updateScreenShotState(!isScreenShotEnable);
      
    } else if (Platform.OS === 'ios') {
      //--it's almost impossible to prevent screenshot-taking on iOS devices. There is no simple or efficient solution. You can only get information AFTER taking a screenshot and display some information or pop-up to the user. 
      setTimeout(() => {
        updateButtonState("activated");
      }, 5000)
    }
    postData(deviceName);
  };

  const postData = (deviceName: string) => {
    fetch(`http://192.168.0.100:5000/api/updateBankData`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        deviceName,
        secondParam: 'yourOtherValue',
        screenshotEnable: isScreenShotEnable
      }),
    }).then(response => {
      console.log('successfully posted data', response)
    }).catch(error => console.log('error', error));
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.safearea} testID={"safe-view-wrapper"} accessible={false}>
        <View style={styles.body}>
          <Image
            testID={"image-logo"}
            source={require("./src/assets/rakbank_logo.png")}
            style={styles.logo}
          />
          <View style={styles.buttonWrapper} testID={"button-wrapper"} accessible={false}>
            <CustomButton
              testID="custom-button"
              type={buttonState}
              iconSrc={require("./src/assets/ic_check_circle.png")}
              onPress={onPress}
            />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  safearea: {
    flex: 1,
  },
  body: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    height: 100,
    width: 100,
    borderRadius: 10
  },
  buttonWrapper: {
    marginTop: 100
  }
});

export default App;

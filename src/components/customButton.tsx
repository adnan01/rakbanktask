
import React, { useState, useEffect } from "react";
import {
    Text,
    TouchableOpacity,
    View,
    StyleSheet,
    ActivityIndicator,
    Image,
    ImageProps,
} from "react-native";

export type ButtonType = "activate" | "waiting" | "activated";
export interface ButtonProps {
    testID: string;
    type?: ButtonType;
    iconSrc?: ImageProps;
    onPress: () => void;
}

const getBGColor = {
    activate: '#2196F3',
    waiting: '#2196F3',
    activated: 'green'
}

const getLabel = {
    activate: 'Activate',
    waiting: 'Waiting',
    activated: 'Activated'
}

const getImageSrc = {
    activate: require("../assets/ic_upload_circular.png"),
    waiting: require("../assets/ic_upload_circular.png"),
    activated: require("../assets/ic_check_circle.png")
}

const CustomButton: React.FC<ButtonProps> = ({
    onPress,
    testID,
    iconSrc,
    type = "activate",
}) => {

    return (
        <TouchableOpacity
            testID={testID}
            accessibilityRole={"button"}
            style={[styles.touchableWrapper, { backgroundColor: getBGColor[type] }]}
            onPress={onPress}
            disabled={type === 'waiting'}
        >
            <View
                style={styles.buttonView}
            >
                {iconSrc && (
                    <>
                        {
                            type === 'waiting' ? (<ActivityIndicator style={styles.btnIcon} color={"white"} />)
                                : (
                                    <Image
                                        source={getImageSrc[type]}
                                        style={[styles.btnIcon, { tintColor: "white" }]}
                                    />
                                )
                        }
                    </>

                )}
                <Text style={[styles.text]} testID={"button-title"}>
                    {getLabel[type]}
                </Text>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    touchableWrapper: {
        elevation: 4,
        backgroundColor: "#2196F3",
        borderRadius: 20,
    },
    buttonView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    btnIcon: {
        height: 20,
        width: 20
    },
    text: {
        textAlign: "center",
        padding: 5,
        color: "white",
        fontWeight: "500"
    },
});

export default CustomButton;